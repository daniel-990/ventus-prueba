import React, { Component } from 'react';
import HeadPage from '../components/head';
import NavBar from '../components/navbar';

//src
import ImagenFondo from '../src/fondo.png';
import ImgElemento from '../src/pagina-web.png';

class IndexPage extends Component {
  render() {
    return (
      <div>
        <HeadPage />
        <NavBar />
        <div className="container-prpal">

          <div className="container">
            <img className="img-responsive img-elemento" src={ImgElemento} />
            <div className="contdor">
              <h1 className="text-center fuente-home titulo">Mi Empresa</h1>
              <span>Lorem Ipsun, Lorem Ipsun</span>

              <hr className="bordo"/>

              <div className="row">
                <div className="col-md-4">
                  <a className="btn btn-pagina" href=""><i className="fab fa-twitter"></i> Twitter</a>
                </div>
                <div className="col-md-4">
                  <a className="btn btn-pagina" href=""><i className="fab fa-github"></i> Github</a>
                </div>
                <div className="col-md-4">
                  <a className="btn btn-pagina" href=""><i className="fab fa-linkedin-in"></i> Linkedin</a>
                </div>
              </div>
            </div>
          </div>

        </div>

        <style jsx>
          {`
            .bordo{
              width: 8%;
              position: relative;
              margin: 0px;
              margin-bottom: 15px;
              border: 1px solid #6fb6f8;
            }
            .titulo{
              color: #5450b8;
              font-size: 35px;
              text-align: left !important;
              margin-bottom: 18px;
              font-family: 'Noto Sans', sans-serif;
            }
            .container-prpal{
              background-image: url(${ImagenFondo});
              background-repeat: no-repeat;
              background-attachment: fixed;
              background-size: cover;
              position: absolute;
              width: 100%;
              height: 100%;
            }
            .contdor{
              position:relative;
              top: 100px;
            }
            .img-elemento{
              width: 650px;
              float: right;
              position:relative;
              top:84px;
            }
            .btn-pagina{
              background-color: #5450b8;
              border-radius: 100px;
              color: white;
              border: none;
              text-align: center;
              width: 100%;
            }
          `}
        </style>

      </div>
    )
  }
}


export default IndexPage;
