import React, { Component } from 'react';
import Link from 'next/link';


//src
import LogoElemento from '../src/logo.png';

class NavBar extends Component {
    render() {

        function navLink() {
            return [
                {
                    id: 1,
                    nombre: 'Inicio',
                    link: '/',
                    clase: 'nav-link activo'
                },
                {
                    id: 2,
                    nombre: 'Servicios',
                    link: '/servicios',
                    clase: 'nav-link'
                },
                {
                    id: 3,
                    nombre: 'Contacto',
                    link: '/contacto',
                    clase: 'nav-link'
                }
            ]
        }

        const MenuLink = navLink().map((items, i) => {
            return (
                <li key={i} className="nav-item">
                    <style jsx>
                        {`
                        .activo{
                            color: #5450b8 !important;
                        }
                    `}
                    </style>
                    <Link href={items.link}><a className={items.clase}>{items.nombre}</a></Link>
                </li>
            )
        })


        return (
            <div>
                <nav className="navbar navbar-expand-lg navbar-light bg-light">
                    <a className="navbar-brand" href="#">
                        <img className="img-responsive logo" src={LogoElemento} />
                    </a>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>

                    <div className="collapse navbar-collapse" id="navbarNavDropdown">
                        <ul className="nav navbar-nav ml-auto">
                            {MenuLink}
                        </ul>
                    </div>

                </nav>
                <style jsx>
                    {`
                    .logo{
                        width: 45px;
                        padding: 1px;
                    }
                `}
                </style>
            </div>
        )
    }
}


export default NavBar;