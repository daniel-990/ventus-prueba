import React, { Component } from 'react';

import HeadePage from '../components/head';
import CalculadoraOb from '../components/calculadora';

class IndexPage extends Component {
  render() {
    return (
      <div>
        <HeadePage />
        <CalculadoraOb />
      </div>
    )
  }
}

export default IndexPage;
