import React, { Component } from 'react';


const mth = Math.floor;

class CalculadoraObj extends Component {


    constructor(props) {
        super(props);
        this.state = {
            valor1: '',
            valor2: '',
            valor3: ''
        }
        //tomo los datos
        this.handleChange = this.handleChange.bind(this);

        //creo mi evento suma
        this.oprsuma = this.oprsuma.bind(this);
        this.oprResta = this.oprResta.bind(this);
        this.oprMultiplicacion = this.oprMultiplicacion.bind(this);
        this.oprDivisor = this.oprDivisor.bind(this);
        this.oprPorcentaje = this.oprPorcentaje.bind(this);
        this.oprRaiz = this.oprRaiz.bind(this);
    }

    handleChange(key) {
        return function (e) {
            var state = {}
            state[key] = e.target.value;
            this.setState(state);
            console.log(state);
        }.bind(this);
    }

    oprsuma(event) {

        const obj1 = mth(parseFloat(this.state.valor1));
        const obj2 = mth(parseFloat(this.state.valor2));

        const suma = obj1 + obj2;
        document.getElementById('resultado').innerHTML = suma;
        event.preventDefault();
    }

    oprResta(event) {

        const obj1 = mth(parseFloat(this.state.valor1));
        const obj2 = mth(parseFloat(this.state.valor2));
        const resta = obj1 - obj2;

        console.log(obj1 - obj2);
        document.getElementById('resultado').innerHTML = resta;
        event.preventDefault();
    }

    oprMultiplicacion(event) {
        const obj1 = mth(parseFloat(this.state.valor1));
        const obj2 = mth(parseFloat(this.state.valor2));
        const multiplicacion = obj1 * obj2;

        document.getElementById('resultado').innerHTML = multiplicacion;
        event.preventDefault();
    }

    oprDivisor(event) {
        const obj1 = mth(parseFloat(this.state.valor1));
        const obj2 = mth(parseFloat(this.state.valor2));
        const division = obj1 / obj2;

        document.getElementById('resultado').innerHTML = division;
        event.preventDefault();
    }

    oprRaiz(event) {
        const obj3 = mth(parseFloat(this.state.valor3));
        const raiz = Math.sqrt(obj3);
        document.getElementById('resultado-r').innerHTML = raiz;
        console.log(raiz);
        event.preventDefault();
    }

    oprPorcentaje(event){
        const obj1 = mth(parseFloat(this.state.valor1));
        const obj2 = mth(parseFloat(this.state.valor2));
        const porcentaje = (obj1/obj2)*100;

        document.getElementById('resultado').innerHTML = porcentaje;
        event.preventDefault();
    }



    render() {
        return (
            <div>
                <style jsx>
                    {`
                        .contentcalculadora{
                            padding-top:30px;
                        }
                        .btnr{
                            width: 100%;
                            padding: 8px;
                            margin: 1px;
                        }
                        .botonera{
                            position: relative;
                            left: 0px;
                            width: 100%;
                        }
                        .resultado{
                            padding-bottom: 10px;
                            font-weight: 700;
                        }
                        #cnt-resultado, #cnt-resultado-r{    
                            padding: 0;
                        }
                        .resultado-raiz{
                            margin-top:10px;
                        }
                        .titulo{
                            position:relative;
                            left:-15px;
                        }
                    `}
                </style>
                <div className="container">
                    <div className="row">
                        <div className="col-md-6">
                            <div className="contentcalculadora">
                                <form>
                                    <h3 className="titulo">Calculadora</h3>
                                    <div className="form-group row">

                                        <input type="number" className="form-control col-md-4" value={this.state.valor1} placeholder="valor 1" onChange={this.handleChange('valor1')} />
                                        <br /><br />
                                        <input type="number" className="form-control col-md-4" value={this.state.valor2} placeholder="valor 2" onChange={this.handleChange('valor2')} />
                                        <br /><br />
                                        <div id="cnt-resultado" className="container resultado">
                                            <h3><i className="fas fa-calculator"></i> <i className="fas fa-equals"></i> <span id="resultado"></span> </h3>
                                        </div>

                                        <div className="botonera row">
                                            <div className="col-md-4">
                                                <button type='submit' className="btn btn-danger btnr" onClick={this.oprsuma}><i className="fas fa-plus-circle"></i></button>
                                            </div>
                                            <div className="col-md-4">
                                                <button type='submit' className="btn btn-danger btnr" onClick={this.oprResta}><i className="fas fa-minus"></i></button>
                                            </div>
                                            <div className="col-md-4">
                                                <button type='submit' className="btn btn-danger btnr" onClick={this.oprMultiplicacion}><i className="fas fa-times"></i></button>
                                            </div>
                                            <div className="col-md-4">
                                                <button type='submit' className="btn btn-danger btnr" onClick={this.oprDivisor}><i className="fas fa-divide"></i></button>
                                            </div>
                                            <div className="col-md-4">
                                                <button type='submit' className="btn btn-danger btnr" onClick={this.oprPorcentaje}><i className="fas fa-percentage"></i></button>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="form-group row">
                                        <input type="number" className="form-control col-md-4" value={this.state.valor3} placeholder="valor 3" onChange={this.handleChange('valor3')} />
                                        <button type="submit" className="btn btn-success" onClick={this.oprRaiz}><i className="fas fa-square-root-alt"></i></button>
                                        
                                        <div id="cnt-resultado-r" className="container resultado resultado-raiz">
                                            <h3><i className="fas fa-calculator"></i> <i className="fas fa-equals"></i> <span id="resultado-r"></span> </h3>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div id="reusltado">....</div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


export default CalculadoraObj;